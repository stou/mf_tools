#!/usr/bin/env python

"""
Layers the fv and enuc together
"""


import argparse
import os
import re
import subprocess


#temp_template = '/u/sciteam/sandalsk/scratch/timed/%s'

def join_vtk(output_prefix, input_prefix, input_count, dump_number):
  """
  """

  image_path = '.'

  filename_out = "merged/%s.%04d.vtk" % (output_prefix, dump_number)
  inputs = []

  for i in range(input_count):
    if i == 0:
#      inputs.append("%s.%04d.vtk" % (input_prefix, dump_number))
      inputs.append("id0/%s.%04d.vtk" % (input_prefix, dump_number))
    else:
#      inputs.append("%s-id%i.%04d.vtk" % (input_prefix, i, dump_number))
      inputs.append("id%i/%s-id%i.%04d.vtk" % (i,input_prefix, i, dump_number))

  args = ['./join_vtk', '-o', filename_out] + inputs

  if os.path.exists(filename_out):
    print "%s exists, skipping " % filename_out
    return

  print args

  p = subprocess.Popen(args, cwd=image_path)
  p.wait()
  return

def build_argparse():

  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('-o', help='Output Prefix', required=True)
  parser.add_argument('-i', help='Input Prefix', required=True)
  parser.add_argument('--dumps', help='Bottom image', required=True, type=int)
  parser.add_argument('--chunks', help='Number of chunks', required=True, type=int)

  return parser

def main():

  parser = build_argparse()
  args = parser.parse_args()

  for dump in range(args.dumps):
    join_vtk(args.o, args.i, args.chunks, dump)


if __name__ == '__main__':
  main()
